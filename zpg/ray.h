#ifndef RAY_H_
#define RAY_H_

/*! \struct Ray
\brief Paprsek.

\f$\mathbf{r}(t) = O + \hat{\mathbf{d}}t\f$

\author Tom� Fabi�n
\version 1.0
\date 2011-2013
*/
struct Ray
{
	Vector3 origin; /*!< Po��tek paprsku. */
	Vector3 direction; /*!< Sm�rov� vektor (jednotkov�). */
	Vector3 inv_direction; /*!< Invertovan� sm�rov� vektor. */	
	TYPE_REAL t; /*!< Re�ln� parametr \f$tf$. */
	Triangle * target; /*!< Ukazatel na zasa�en� troj�heln�ky. */
	TYPE_REAL env_ior; /*!< Index lomu prost�ed�, kter�m se paprsek aktu�ln� pohybuje. */
	char direction_sign[3]; /*!< Znam�nko sm�rov�ho vektoru. */	

	//! Specializovan� konstruktor.
	/*!
	Inicializuje paprsek podle zadan�ch hodnot a jdouc� do nekone�na.

	\param origin po��tek.
	\param direction jednotkov� sm�r.
	*/
	Ray( const Vector3 & origin, const Vector3 & direction, const TYPE_REAL bounce = EPSILON, const TYPE_REAL env_ior = IOR_AIR )
	{
		this->origin = origin;
		set_direction( direction );		

		this->origin += bounce * this->direction;

		t = REAL_MAX;
		target = NULL;

		this->env_ior = env_ior;

#pragma omp atomic
		++no_rays_;
	}

	//! Vypo�te \f$\mathbf{r}(t)\f$.
	/*!	
	\return Sou�adnice bodu na paprsku pro zadan� parametr \f$t\f$.
	*/
	Vector3 eval( const TYPE_REAL t )
	{
		return origin + direction * t;
	}

	//! Vypo�te \f$r(t)\f$.
	/*!	
	\return True je-li \a t men�� ne� .
	*/
	bool closest_hit( const TYPE_REAL t, Triangle * const triangle )
	{
		bool hit_confirmed = false;

		//#pragma omp critical ( make_hit )
		{
			if ( ( t < this->t ) && ( t > 0 ) )
			{
				this->t = t;
				hit_confirmed = true;
				target = triangle;
			}
		}

		return hit_confirmed;
	}

	bool is_hit() const
	{
		return ( ( t > 0 ) && ( t < REAL_MAX ) && ( target != NULL ) );
	}

	void set_direction( const Vector3 & direction )
	{
		this->direction = direction;
		this->direction.Normalize();
		inv_direction = Vector3( 1 / this->direction.x, 1 / this->direction.y, 1 / this->direction.z );
		direction_sign[0] = static_cast<char>( inv_direction.x < 0 ); // 0 pro <0,+inf> a 1 pro <-inf,0)
		direction_sign[1] = static_cast<char>( inv_direction.y < 0 );
		direction_sign[2] = static_cast<char>( inv_direction.z < 0 );
	}

	RTCRay getRTCRay() {
		RTCRay rtc_ray;

		// nastaven� parametr� paprsku
		rtc_ray.org[0] = origin.x;
		rtc_ray.org[1] = origin.y;
		rtc_ray.org[2] = origin.z;

		rtc_ray.dir[0] = direction.x;
		rtc_ray.dir[1] = direction.y;
		rtc_ray.dir[2] = direction.z;

		rtc_ray.tnear = 0.0f;
		rtc_ray.tfar = std::numeric_limits<float>::infinity();

		rtc_ray.geomID = RTC_INVALID_GEOMETRY_ID;
		rtc_ray.primID = RTC_INVALID_GEOMETRY_ID;
		rtc_ray.instID = RTC_INVALID_GEOMETRY_ID;

		rtc_ray.mask = 0xFFFFFFFF;
		rtc_ray.time = 0.0f;

		return rtc_ray;
	}

	static bool visibilityTest(RTCScene& scene, Vector3 p, Vector3 dst) {
		RTCRay rtc_ray;

		// nastaven� parametr� paprsku
		rtc_ray.org[0] = p.x;
		rtc_ray.org[1] = p.y;
		rtc_ray.org[2] = p.z;

		rtc_ray.tnear = 0.f + EPSILON;
		rtc_ray.tfar = dst.L2Norm() - EPSILON;

		rtc_ray.dir[0] = dst.x;
		rtc_ray.dir[1] = dst.y;
		rtc_ray.dir[2] = dst.z;

		rtc_ray.geomID = RTC_INVALID_GEOMETRY_ID;
		rtc_ray.primID = RTC_INVALID_GEOMETRY_ID;
		rtc_ray.instID = RTC_INVALID_GEOMETRY_ID;

		rtc_ray.mask = 0xFFFFFFFF;
		rtc_ray.time = 0.0f;

		rtcOccluded(scene, rtc_ray);

		return rtc_ray.geomID == RTC_INVALID_GEOMETRY_ID;
	}

	static RTCRay createShadowRay(Vector3 & p, Vector3 & lightDir) {
		RTCRay rtc_ray;

		// nastaven� parametr� paprsku
		rtc_ray.org[0] = p.x;
		rtc_ray.org[1] = p.y;
		rtc_ray.org[2] = p.z;

		rtc_ray.tnear = 0.001f;
		rtc_ray.tfar = 1.f;

		/*rtc_ray.tnear = 0.001f;
		rtc_ray.tfar = lightDir.L2Norm();

		lightDir.Normalize();*/

		rtc_ray.dir[0] = lightDir.x;
		rtc_ray.dir[1] = lightDir.y;
		rtc_ray.dir[2] = lightDir.z;

		rtc_ray.geomID = RTC_INVALID_GEOMETRY_ID;
		rtc_ray.primID = RTC_INVALID_GEOMETRY_ID;
		rtc_ray.instID = RTC_INVALID_GEOMETRY_ID;

		rtc_ray.mask = 0xFFFFFFFF;
		rtc_ray.time = 0.0f;

		return rtc_ray;
	}

	static Vector2* generateRayCastPositions(const int &numberOfRays, const int &matrixSize, const int &pixelX, const int &pixelY, const float &pixelCoeficient) {
		Vector2 *rayCastPos = new Vector2[numberOfRays];

		for(int r = 0; r < matrixSize; r++) {
			for(int c = 0; c < matrixSize; c++) {
				float xmin = pixelX + c*pixelCoeficient;
				float xmax = xmin + pixelCoeficient;
				float ymin = pixelY + r*pixelCoeficient;
				float ymax = ymin + pixelCoeficient;
				rayCastPos[ matrixSize * r + c ] = RandomVec2(xmin, xmax, ymin, ymax);
			}
		}

		return rayCastPos;
	}

	static void no_rays_reset()
	{
		no_rays_ = 0;
	}

	static long long no_rays()
	{
		return no_rays_;
	}

private:
	static long long no_rays_;
};

#endif
