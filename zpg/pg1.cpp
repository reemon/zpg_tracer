#include "stdafx.h"

void rtc_error_function( const RTCError code, const char * str )
{
	printf( "ERROR in Embree: %s\n", str );
	exit( 1 );
}

RTCError check_rtc_or_die( RTCDevice & device )
{
	const RTCError error = rtcDeviceGetError( device );

	if ( error != RTC_NO_ERROR )
	{
		printf( "ERROR in Embree: " );

		switch ( error )
		{
		case RTC_UNKNOWN_ERROR:
			printf( "An unknown error has occurred." );
			break;

		case RTC_INVALID_ARGUMENT:
			printf( "An invalid argument was specified." );
			break;

		case RTC_INVALID_OPERATION:
			printf( "The operation is not allowed for the specified object." );
			break;

		case RTC_OUT_OF_MEMORY:
			printf( "There is not enough memory left to complete the operation." );
			break;

		case RTC_UNSUPPORTED_CPU:
			printf( "The CPU is not supported as it does not support SSE2." );
			break;

		case RTC_CANCELLED:
			printf( "The operation got cancelled by an Memory Monitor Callback or Progress Monitor Callback function." );
			break;
		}

		fflush( stdout );
		exit( 1 );
	}

	return error;
}

// struktury pro ukládání dat pro Embree
namespace embree_structs
{
	struct Vertex { float x, y, z, a; };
	typedef Vertex Normal;
	struct Triangle { int v0, v1, v2; };
};

/*
Seznam úkolů:

1, Doplnit TODO v souboru tracing.cpp.
*/

int main( int argc, char * argv[] )
{
	printf( "PG1, (c)2011-2015 Tomas Fabian\n\n" );

	_MM_SET_FLUSH_ZERO_MODE( _MM_FLUSH_ZERO_ON ); // Flush to Zero, Denormals are Zero mode of the MXCSR
	_MM_SET_DENORMALS_ZERO_MODE( _MM_DENORMALS_ZERO_ON );	
	RTCDevice device = rtcNewDevice( NULL ); // musíme vytvořit alespoň jedno Embree zařízení		
	check_rtc_or_die( device ); // ověření úspěšného vytvoření Embree zařízení
	rtcDeviceSetErrorFunction( device, rtc_error_function ); // registrace call-back funkce pro zachytávání chyb v Embree

	// načtení geometrie
	std::vector<Surface *> surfaces;
	std::vector<Material *> materials;
	if ( LoadOBJ( "../../data/twospheres.obj", Vector3( 0.5f, 0.5f, 0.5f ), surfaces, materials) < 0)
	{
		return -1;
	}

	const bool USE_CUBE_MAP = false;

	std::string cubemapType = "city";
	std::string textures[] = {
		"../../data/"+cubemapType+"/posx.jpg",
		"../../data/"+cubemapType+"/negx.jpg",
		"../../data/"+cubemapType+"/posy.jpg",
		"../../data/"+cubemapType+"/negy.jpg",
		"../../data/"+cubemapType+"/posz.jpg",
		"../../data/"+cubemapType+"/negz.jpg"
	};

	CubeMap* citycubeMap = new CubeMap(textures, USE_CUBE_MAP); 

	std::string pcubemapType = "park";
	std::string ptextures[] = {
		"../../data/"+pcubemapType+"/posx.jpg",
		"../../data/"+pcubemapType+"/negx.jpg",
		"../../data/"+pcubemapType+"/posy.jpg",
		"../../data/"+pcubemapType+"/negy.jpg",
		"../../data/"+pcubemapType+"/posz.jpg",
		"../../data/"+pcubemapType+"/negz.jpg"
	};

	CubeMap* parkcubeMap = new CubeMap(ptextures, USE_CUBE_MAP); 

	// vytvoření kamery
	//z (-)dolu, nahoru(+)
	//x (+) doleva, (-) doprava
	//y (+) dozadu, (-) dopredu
	//Vector3( -1.5f, -3.0f, 2.f )
	Camera camera = Camera( 800, 600, Vector3( -1.5f, -3.0f, 2.f ), Vector3( 0.0f, 0.f, 0.5f ), DEG2RAD(40.f));
	
	// vytvoření světel	
	std::vector<OmniLight *> lights;
	lights.push_back( new OmniLight(
		Vector3( 3.15f, 3.33f, 6.25f ),
		Vector3( 0.75f, 0.75f, 0.75f ),
		Vector3( 0.5f, 0.5f, 0.5f ),
		Vector3( 0.75f, 0.75f, 0.75f ) 
	));
	lights.push_back( new OmniLight(
		Vector3( 3.15f, -3.33f, 6.25f ),
		Vector3( 0.75f, 0.75f, 0.75f ),
		Vector3( 0.5f, 0.5f, 0.5f ),
		Vector3( 0.75f, 0.75f, 0.75f ) 
	));

	// vytvoření scény v rámci Embree
	RTCScene scene = rtcDeviceNewScene( device, RTC_SCENE_STATIC | RTC_SCENE_HIGH_QUALITY, RTC_INTERSECT1 | RTC_INTERPOLATE );

	// nakopírování všech modelů do bufferů Embree
	for ( std::vector<Surface *>::const_iterator iter = surfaces.begin();
		iter != surfaces.end(); ++iter )
	{
		Surface * surface = *iter;
		unsigned geom_id = rtcNewTriangleMesh( scene, RTC_GEOMETRY_STATIC,
			surface->no_triangles(), surface->no_vertices() );

		// kopírování vertexů
		embree_structs::Vertex * vertices = static_cast< embree_structs::Vertex * >(
			rtcMapBuffer( scene, geom_id, RTC_VERTEX_BUFFER ) );

		for ( int t = 0; t < surface->no_triangles(); ++t )
		{
			for ( int v = 0; v < 3; ++v )
			{
				embree_structs::Vertex & vertex = vertices[t * 3 + v];

				vertex.x = surface->get_triangles()[t].vertex( v ).position.x;
				vertex.y = surface->get_triangles()[t].vertex( v ).position.y;
				vertex.z = surface->get_triangles()[t].vertex( v ).position.z;
			}
		}

		rtcUnmapBuffer( scene, geom_id, RTC_VERTEX_BUFFER );

		// vytváření indexů vrcholů pro jednotlivé trojúhelníky
		embree_structs::Triangle * triangles = static_cast< embree_structs::Triangle * >(
			rtcMapBuffer( scene, geom_id, RTC_INDEX_BUFFER ) );

		for ( int t = 0, v = 0; t < surface->no_triangles(); ++t )
		{
			embree_structs::Triangle & triangle = triangles[t];

			triangle.v0 = v++;
			triangle.v1 = v++;
			triangle.v2 = v++;
		}

		rtcUnmapBuffer( scene, geom_id, RTC_INDEX_BUFFER );		
	}

	rtcCommit( scene );

	RadiosityMethod* radiosity = new RadiosityMethod(&scene, &surfaces, citycubeMap);
	radiosity->setImportenceMethod(true);
	radiosity->useMaterialColor(false);
	radiosity->useCubeMap(false);
	radiosity->useBRDFPhong(true);
	
	Tracer imphongTracer("importanceWithPhong", &camera);
	imphongTracer.setSuperSampling(81);
	imphongTracer.setScalineSamples(16);
	imphongTracer.setTraceMethod(radiosity);
	imphongTracer.trace();

	radiosity->useMaterialColor(true);
	radiosity->useCubeMap(true);

	Tracer imphongMaterialParkTracer("importanceWithPhongAndMaterialPark", &camera);
	imphongMaterialParkTracer.setSuperSampling(81);
	imphongMaterialParkTracer.setScalineSamples(16);
	imphongMaterialParkTracer.setTraceMethod(radiosity);
	imphongMaterialParkTracer.trace();

	cv::waitKey( 0 );
	cv::destroyAllWindows();

	rtcDeleteScene( scene ); // zrušení Embree scény

	SafeDeleteVectorItems<Material *>( materials );
	SafeDeleteVectorItems<Surface *>( surfaces );

	rtcDeleteDevice( device ); // Embree zařízení musíme také uvolnit před ukončením aplikace

	return 0;
}
