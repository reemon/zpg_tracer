#include "stdafx.h"

RadiosityMethod::RadiosityMethod(RTCScene* scene, std::vector<Surface *>* surfaces, CubeMap* cubeMap) {
	this->init();

	this->scene = scene;
	this->surfaces = surfaces;
	this->cubeMap = cubeMap;
	this->importenceSampleMethod = false;
	this->useMaterialColorValue = true;
	this->useBrdfPhong = false;
}

float RadiosityMethod::BRDF_Lambert(const Vector3& wi, const Vector3& wo, float albedo) {
	return albedo / static_cast<float>(M_PI);
}

float RadiosityMethod::BRDF_Phong(const Vector3& wi, const Vector3& ray_dir, const Vector3& normal,  float ambient, float diffuse, float specular, float shininess ) {
	//wi = ray direction
	Vector3 l_d = wi;
	l_d.Normalize();

	Vector3 l_r = l_d.Reflect(normal);

	return ambient + diffuse * MAX(0, normal.DotProduct(l_d)) + specular * MAX(0, powf(l_r.DotProduct(ray_dir), shininess));
}

Vector3 RadiosityMethod::importenceSampleHemisphere(Vector3& normal, Vector3 ray_direction) {	
	normal = (ray_direction.DotProduct(normal)) >= 0.f ? normal : (-1.f*normal);
	return importenceSampleHemisphere(normal);
}

Vector3 RadiosityMethod::importenceSampleHemisphere(Vector3& normal) {
	Vector3 omega_i = Vector3();

	const float r1 = Random();
	const float r2 = Random();

	const float x = cos(2*M_PI*r1)*sqrt(1-r2);
	const float y = sin(2*M_PI*r1)*sqrt(1-r2);
	const float z = sqrt(r2);

	Vector3 tx = normal.ortho();
	tx.Normalize();
	Vector3 ty = tx.CrossProduct(normal);
	ty.Normalize();

	Matrix4 transformMatrix = Matrix4(tx.x, ty.x, normal.x, 0,
									  tx.y, ty.y, normal.y, 0,
									  tx.z, ty.z, normal.z, 0,
									  0, 0, 0, 1);
	
	omega_i = transformMatrix*Vector3(x, y, z);

	return omega_i;
}



Vector3 RadiosityMethod::sampleHemisphere(Vector3& normal, Vector3 ray_direction) {
	normal = (ray_direction.DotProduct(normal)) >= 0.f ? normal : (-1.f*normal);
	return sampleHemisphere(normal);
}

Vector3 RadiosityMethod::sampleHemisphere(Vector3& normal) {
	Vector3 omega_i = Vector3();
	
	const float r1 = Random();
	const float r2 = Random();

	const float x = cos(2*M_PI*r1)*sqrt(1-SQR(r2));
	const float y = sin(2*M_PI*r1)*sqrt(1-SQR(r2));
	const float z = r2;

	Vector3 tx = normal.ortho();
	tx.Normalize();
	Vector3 ty = tx.CrossProduct(normal);
	ty.Normalize();

	Matrix4 transformMatrix = Matrix4(tx.x, ty.x, normal.x, 0,
									  tx.y, ty.y, normal.y, 0,
									  tx.z, ty.z, normal.z, 0,
									  0, 0, 0, 1);
										
	return transformMatrix*Vector3(x, y, z);
}

Vector3 RadiosityMethod::trace(Ray& ray) {
	Color4 color = Color4(1.f, 1.f, 1.f, 1.f);
	if(cubeMap != nullptr && this->useCubeMapTexture) {
		color = cubeMap->get_texel(ray.direction);
	}

	Vector3 l_o = Vector3(color.r, color.g, color.b);// výsledná barva aktuálního pixelu

	RTCRay rtc_ray = ray.getRTCRay();
	rtcIntersect( *scene, rtc_ray );

	if ( rtc_ray.geomID != RTC_INVALID_GEOMETRY_ID )
	{
		l_o = Vector3();

		Surface* surface = surfaces->at(rtc_ray.geomID);
		Triangle &triangle = surface->get_triangles()[rtc_ray.primID];
		Material *material = surface->get_material();

		//hit point
		Vector3 p = ray.eval(rtc_ray.tfar);
		Vector2 texCoord = triangle.texture_coord(rtc_ray.u, rtc_ray.v);

		Vector3 normal = triangle.normal(rtc_ray.u, rtc_ray.v);
	
		Vector3 ambient = material->ambient;
		Vector3 diffuse = material->diffuse;
		Vector3 specular = material->specular;
		float shininess = material->shininess;

		Vector3 materialColor = Vector3(1.f, 1.f, 1.f);
		if(useMaterialColorValue) {
			Texture *tex_diffuse = material->get_texture(Material::kDiffuseMapSlot);
			Texture *tex_specular = material->get_texture(Material::kSpecularMapSlot);

			if(tex_diffuse != NULL) {
				Color4 texel = tex_diffuse->get_texel(texCoord.x, texCoord.y);
				diffuse = Vector3(texel.r, texel.g, texel.b);
			}
			if(tex_specular != NULL) {
				Color4 texel = tex_specular->get_texel(texCoord.x, texCoord.y);
				specular = Vector3(texel.r, texel.g, texel.b);
			}

			materialColor = ambient+diffuse+specular;
		}

		Vector3 omega_o = -ray.direction;
		float visibility = 0.f;

		//direct light
		{
			Vector3 omega_i = importenceSampleMethod ? importenceSampleHemisphere(Vector3(0.f, 0.f, 1.f)) : sampleHemisphere(Vector3(0.f, 0.f, 1.f));

			Vector3 l_i = Vector3(1.f, 1.f, 1.f);
			float cosPhi = normal.DotProduct(omega_i);
			float pdf = 1.f / (2*M_PI);
			float K = useBrdfPhong ? BRDF_Phong(omega_i, ray.direction, normal) : BRDF_Lambert(omega_i, omega_o);

			if(importenceSampleMethod) {
				 pdf = (cosPhi / M_PI);
			}

			visibility = Ray::visibilityTest(*scene, p, p+100.f*omega_i) ? 1.f : 0.f;

			l_o += (visibility * l_i * K * cosPhi * materialColor) / pdf;
		}
		
		//indirect light
		if(visibility != 1.f){		
			Vector3 omega_i = importenceSampleMethod ? importenceSampleHemisphere(normal, omega_o) : sampleHemisphere(normal, omega_o);

			Ray reflectedRay = Ray(p, omega_i);

			Vector3 l_i = this->trace(reflectedRay);
			float K = useBrdfPhong ? BRDF_Phong(omega_i, ray.direction, normal) : BRDF_Lambert(omega_i, omega_o);
			float cosPhi = normal.DotProduct(omega_i);
			float pdf = 1.f / (2*M_PI);

			if(importenceSampleMethod) {
				 pdf = (cosPhi / M_PI);
			}

			l_o += ((1.f-visibility)*l_i * K * cosPhi * materialColor) / pdf;
		}
	}

	return l_o;
}