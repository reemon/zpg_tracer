#ifndef CUBEMAP_H_
#define CUBEMAP_H_

class CubeMap {
private:
	Texture** maps;
	Vector3 direction;
	bool use;


	int hitSide(Vector3& dir);

public:
	CubeMap(std::string texturesPath[], bool use = true);
	~CubeMap();

	Color4 get_texel(Vector3 &direction);
	bool shouldUse() { return use;};
};

#endif