#ifndef RAYTRACEMETHOD_H_
#define RAYTRACEMETHOD_H_

class RayTraceMethod : public TraceMethod {
private:
	CubeMap* cubeMap;
	RTCScene* scene;
	std::vector<Surface *>* surfaces;
	std::vector<OmniLight *>* lights;

	int maxDepth;
	int currentDepth;

public:
	RayTraceMethod(RTCScene* scene, std::vector<Surface *>* surfaces, std::vector<OmniLight *>* lights, CubeMap* cubeMap = nullptr);

	Vector3 PhongShader(const Vector3 &normal, const Vector3 &p, const Vector2 &text_coord, const Vector3 &rayDir, const Material *mat);
	Vector3 NormalShader( const Vector3 & normal );

	Vector3 trace(Ray& ray);

	void setMaxDepth(int depth) { maxDepth = depth > 0 ? depth : 1; };
};

#endif