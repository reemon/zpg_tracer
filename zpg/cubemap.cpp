#include "stdafx.h"

CubeMap::CubeMap(std::string texturePaths[], bool use) {
	int flip[] = {1,-1,-1,-1,-1,-1};
	maps = new Texture*[6];
	for(int i = 0; i < 6; i++) {
		maps[i] = LoadTexture(texturePaths[i].c_str(), flip[i]);
	}

	this->use = use;
}

CubeMap::~CubeMap() {
	for(int i = 0; i < 6; i++) {
		delete maps[i];
	}
	delete maps;
}

int CubeMap::hitSide(Vector3& dir) {
	int n = dir.LargestComponent(true);

	if(n == 0) {
		return dir.x > 0 ? 0 : 1;
	}
	if(n == 1) {
		return dir.y > 0 ? 2 : 3;
	}
	if(n == 2) {
		return dir.z > 0 ? 4 : 5;
	}
	return -1;
}
Color4 CubeMap::get_texel(Vector3& direction) {
	int side = hitSide(direction);

	float u, v, tmp;
	switch(side) {
		//POS_X, NEG_X
		case 0:
		case 1:
			tmp = 1.0f / abs( direction.x );
			u = ( direction.y * tmp + 1 ) * 0.5f; 
			v = ( direction.z * tmp + 1 ) * 0.5f;
			break;
		//POS_Y, NEG_Y
		case 2:
		case 3:
			tmp = 1.0f / abs( direction.y );
			u = ( direction.x * tmp + 1 ) * 0.5f; 
			v = ( direction.z * tmp + 1 ) * 0.5f;
			break;
		//POS_Z, NEG_Z
		case 4:
		case 5:
			tmp = 1.0f / abs( direction.z );
			u = ( direction.x * tmp + 1 ) * 0.5f; 
			v = ( direction.y * tmp + 1 ) * 0.5f;
			break;
		default:
			return Color4(0.f, 0.f, 0.f, 1.f);
	}

	return maps[side]->get_texel( u, v );	
}