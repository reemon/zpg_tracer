#ifndef TRACING_H_
#define TRACING_H_

void Trace( const Camera & camera, std::vector<Surface *> & surfaces );
void RaytraceWithEmbree(std::string window, CubeMap* cubeMap, RTCScene & scene, Camera & camera, std::vector<Surface *> & surfaces, std::vector<OmniLight *> & lights, int nOfIterations = 1, bool importanceSampling = false);
Vector3 RayTrace(CubeMap* cubeMap, RTCScene & scene, Ray &ray, std::vector<Surface *> & surfaces, std::vector<OmniLight *> & lights, int it = 0, int maxIt = 1, int opaqueLevel = 2);
#endif
