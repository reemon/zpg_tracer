#include "stdafx.h"

Tracer::Tracer(std::string window, Camera* camera) {
	this->window = window;
	this->camera = camera;
	this->numberOfRaySamples = -1;
	this->numberOfSamples = -1;
	this->traceMethod = nullptr;

	cv::namedWindow( window, cv::WINDOW_AUTOSIZE);
	cv::moveWindow( window, 0, 0 );
	// obr�zek t�� barevn�ch kan�l�, jeden pixel tvo�� 3 subpixely; Blue, Green, Red
	image = cv::Mat( cv::Size( camera->width(), camera->height() ), CV_32FC3 );
	cv::resizeWindow( window, image.cols, image.rows );
	cv::imshow( window, image );
}

void Tracer::trace() {
	if(traceMethod == nullptr) {
		return;
	}

	rtc_time = 0.0;
	t0 = omp_get_wtime();

	if(numberOfSamples != -1) {
		scaline();
	}else {
		render(0);
	}
}

void Tracer::scaline() {
	//For scaline alghorithm
	for(int s = numberOfSamples; s > 0; s--) {
		render(s);
		cv::waitKey( 5 );
	}
}

void Tracer::superSampling(Vector3& rgb, int x, int y) {
	Vector2* rayCastPos = Ray::generateRayCastPositions(numberOfRaySamples, sampleMatrixSize, x, y, pixelCoeficient);

	for(int i = 0; i < numberOfRaySamples; i++) {
		rgb += traceMethod->trace(camera->GenerateRay(rayCastPos[i].x, rayCastPos[i].y));
	}

	rgb /= numberOfRaySamples;

	delete rayCastPos;
	rayCastPos = nullptr;
}

void Tracer::render(int scaline) {
	int step = scaline <= 0 ? 1 : scaline;
	// projdeme v�echny pixely obrazu		
	for ( int y = scaline; y < image.rows; y += step )
	{
		for ( int x = scaline; x < image.cols; x += step )
		{
			Vector3 rgb = Vector3( 0, 0, 0 );

			//Supersampling off
			if(numberOfRaySamples <= 0) {
				rgb = traceMethod->trace(camera->GenerateRay(x, y));
			} else {
				superSampling(rgb, x, y);
			}

			utils::swap<float>( rgb.data[0], rgb.data[2] ); // OpenCV p�edpol�d� form�t BGR, tak�e p�ehod�me krajn� subpixely

			for(int nY = y-scaline; nY <= y; nY++)
				for(int nX = x-scaline; nX <= x; nX++)
					image.at<Vector3>( nY, nX ) = rgb; // z�pis vypo�ten� barvy pixelu do obr�zku	
		} // x
	} // y

	const double t1 = omp_get_wtime();
	char time[64] = { 0 };
	PrintTime( t1 - t0, time );
	printf( "Scaline number of samples: %d\n", scaline);
	printf( "Traced in %s (i.e. %0.3f Mrays/s, %0.2f fps)\n",
		time, ( Ray::no_rays() / ( t1 - t0 ) ) * 1e-6,
		1.0 / ( t1 - t0 ) );
	PrintTime( rtc_time, time );
	//printf( "%s in Embree (i.e. %0.1f %% of time)\n", time, 100 * rtc_time / ( t1 - t0 ) );

	cv::imshow( window, image );
	cv::imwrite(window, image);
}

void Tracer::setSuperSampling(int samples) {
	numberOfRaySamples = samples > 0 ? samples : 1;
	sampleMatrixSize = static_cast< int >(sqrt(numberOfRaySamples)); 
	pixelCoeficient = 1.f/sampleMatrixSize;
}
