#ifndef TRACEMETHOD_H_
#define TRACEMETHOD_H_

class TraceMethod {
protected:	
	bool useCubeMapTexture;

	void init() { useCubeMapTexture = false; };
public:
	virtual Vector3 trace(Ray& ray) = 0;

	void useCubeMap(bool use){ useCubeMapTexture = use; };
};

#endif