#ifndef TRACER_H_
#define TRACER_H_

class Tracer {
private:
	std::string window; 
	cv::Mat image;
	Camera* camera;

	TraceMethod* traceMethod;
	
	int numberOfSamples; //scaline, from lower to highest resolution

	//Supersampling
	int numberOfRaySamples; //supersempling x rays for one pixel
	int sampleMatrixSize; // 2x2, 3x3, ..., 4 sample rays, 9 sample rays, ... 
	float pixelCoeficient;

	//Timing
	double rtc_time;
	double t0;

	void scaline();
	void superSampling(Vector3& rgb, int x, int y);
	void render(int scaline);
	
public:
	Tracer(std::string window, Camera* camera);

	void trace();

	void setTraceMethod(TraceMethod* method) { traceMethod = method; };
	void setScalineSamples(int scaline) { numberOfSamples = scaline > 0 ? scaline : 1; };
	void setSuperSampling(int samples);

	int getSuperSampling() { return numberOfRaySamples; };
	int getScalineSamples() { return numberOfSamples; };
};

#endif