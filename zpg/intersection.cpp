#include "stdafx.h"

//http://igad.nhtv.nl/~bikker/files/faster.pdf

bool RayTriangleIntersection( Ray & ray, Triangle * triangle )
{
	Vector3 u = triangle->vertex( 1 ).position - triangle->vertex( 0 ).position;
	Vector3 v = triangle->vertex( 2 ).position - triangle->vertex( 0 ).position;

	Vector3 n = -u.CrossProduct( v );
	n.Normalize();

	Vector3 w0 = ray.origin - triangle->vertex( 0 ).position;
	const TYPE_REAL a = -( n.DotProduct( w0 ) );
	const TYPE_REAL b = n.DotProduct( ray.direction );
	const TYPE_REAL t = a / b;

	if ( ( t <= 0 ) || ( t > ray.t ) )
	{
		return false; // paprsek jde opa�n�m sm�rem ne� je troj�heln�k nebo kon�� je�t� p�ed troj�heln�kem
	}

	Vector3 intersection = ray.origin + t * ray.direction; // pr�se��k le�� na rovin� troj�heln�ka

	Vector3 w = intersection - triangle->vertex( 0 ).position;

	const TYPE_REAL uv = u.DotProduct( v );
	const TYPE_REAL uu = u.DotProduct( u );
	const TYPE_REAL vv = v.DotProduct( v );	
	const TYPE_REAL wu = w.DotProduct( u );
	const TYPE_REAL wv = w.DotProduct( v );

	const TYPE_REAL rd = 1 / ( SQR( uv ) - uu * vv );

	const TYPE_REAL s_i = ( uv * wv - vv * wu ) * rd;
	const TYPE_REAL t_i = ( uv * wu - uu * wv ) * rd;

	if ( ( s_i >= 0 ) && ( t_i >= 0 ) && ( s_i + t_i <= 1 ) )
	{	
		return ray.closest_hit( t, triangle );
	}
	
	return false;
}

bool RayTriangleIntersectionMT97( Ray & ray, Triangle * triangle )
{
	Vector3 e1 = triangle->vertex( 1 ).position - triangle->vertex( 0 ).position;
	Vector3 e2 = triangle->vertex( 2 ).position - triangle->vertex( 0 ).position;
	Vector3 p = ray.direction.CrossProduct( e2 );
	const TYPE_REAL a = e1.DotProduct( p );

	if ( ( a > EPSILON ) && ( a < EPSILON ) )
	{
		return false;
	}

	const TYPE_REAL f = 1 / a;
	Vector3 s = ray.origin - triangle->vertex( 0 ).position;
	const TYPE_REAL u = f * ( s.DotProduct( p ) );

	if ( ( u < 0 ) || ( u > 1 ) )
	{
		return false;
	}

	Vector3 q = s.CrossProduct( e1 );
	const TYPE_REAL v = f * ( ray.direction.DotProduct( q ) );

	if ( ( v < 0 ) || ( u + v > 1 ) )
	{
		return false;
	}

	const TYPE_REAL t = f * ( e2.DotProduct( q ) );

	return ray.closest_hit( t, triangle );
}
