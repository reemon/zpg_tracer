#ifndef RADIOSITYMETHOD_H_
#define RADIOSITYMETHOD_H_

class RadiosityMethod : public TraceMethod {
private:
	CubeMap* cubeMap;
	RTCScene* scene;
	std::vector<Surface *>* surfaces;

	bool importenceSampleMethod;
	bool useMaterialColorValue;
	bool useBrdfPhong;

	float BRDF_Lambert(const Vector3& wi, const Vector3& wo, float albedo = 0.5f);
	float BRDF_Phong(const Vector3& wi, const Vector3& ray_dir, const Vector3& normal, float ambient = 0.f, float diffuse = 0.2f, float specular = 0.8f, float shininess = 30.f);

	Vector3 importenceSampleHemisphere(Vector3& normal, Vector3 ray_direction);
	Vector3 importenceSampleHemisphere(Vector3& normal);
	Vector3 sampleHemisphere(Vector3& normal, Vector3 ray_direction);
	Vector3 sampleHemisphere(Vector3& normal);

public:
	RadiosityMethod(RTCScene* scene, std::vector<Surface *>* surfaces, CubeMap* cubeMap = nullptr);

	Vector3 trace(Ray& ray);

	void setImportenceMethod(bool enabled) { importenceSampleMethod = enabled; };
	void useMaterialColor(bool use) { useMaterialColorValue = use; };
	void useBRDFPhong(bool use) { useBrdfPhong = use; };
};

#endif