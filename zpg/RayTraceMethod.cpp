#include "stdafx.h"

RayTraceMethod::RayTraceMethod(RTCScene* scene, std::vector<Surface *>* surfaces, std::vector<OmniLight *>* lights, CubeMap* cubeMap) {
	this->init();

	this->scene = scene;
	this->surfaces = surfaces;
	this->lights = lights;
	this->cubeMap = cubeMap;

	this->maxDepth = 1;
	this->currentDepth = 0;
}

Vector3 RayTraceMethod::PhongShader(const Vector3 &normal, const Vector3 &p, const Vector2 &text_coord, const Vector3 &rayDir, const Material *mat) {
	Vector3 pixel = mat->ambient;

	Texture *tex_diffuse = mat->get_texture(Material::kDiffuseMapSlot);

	Vector3 diffuse = mat->diffuse;
	if(tex_diffuse != NULL) {
		Color4 texel = tex_diffuse->get_texel(text_coord.x, text_coord.y);
		diffuse = Vector3(texel.r, texel.g, texel.b);
	}

	Texture *tex_specular = mat->get_texture(Material::kSpecularMapSlot);

	Vector3 specular = mat->specular;
	if(tex_specular != NULL) {
		Color4 texel = tex_specular->get_texel(text_coord.x, text_coord.y);
		specular = Vector3(texel.r, texel.g, texel.b);
	}

	for(std::vector<OmniLight *>::const_iterator iter = lights->begin(); iter != lights->end(); iter++) {
		OmniLight *light = *iter;
		Vector3 l_d = light->position - p;
		l_d.Normalize();

		//l_d = omega_o
		Vector3 l_r = l_d.Reflect(normal);

		pixel += (1 - light->occlusion) * MAX(0, normal.DotProduct(l_d)) * diffuse;
		pixel += (1 - light->occlusion) * MAX(0, powf(l_r.DotProduct(rayDir), mat->shininess)) * specular;
	}

	return pixel;
}

Vector3 RayTraceMethod::NormalShader( const Vector3 & normal )
{
	return ( Vector3( 1, 1, 1 ) + normal ) * 0.5f;
}

Vector3 RayTraceMethod::trace(Ray& ray) {
	Color4 color = Color4(0.f, 0.f, 0.f, 1.f);

	++currentDepth;

	if(cubeMap != nullptr && this->useCubeMapTexture) {
		color = cubeMap->get_texel(ray.direction);
	}

	Vector3 rgb = Vector3(color.r, color.g, color.b);// výsledná barva aktuálního pixelu

	RTCRay rtc_ray = ray.getRTCRay();

	//const double t0 = omp_get_wtime();
	rtcIntersect( *scene, rtc_ray );
	//const double t1 = omp_get_wtime();
	//rtc_time += t1 - t0;

	if ( rtc_ray.geomID != RTC_INVALID_GEOMETRY_ID )
	{
		Surface* surface = surfaces->at(rtc_ray.geomID);
		Triangle &triangle = surface->get_triangles()[rtc_ray.primID];
		Material *material = surface->get_material();

		//hit point
		Vector3 p = ray.eval(rtc_ray.tfar);
		Vector2 texCoord = triangle.texture_coord(rtc_ray.u, rtc_ray.v);

		//If material has opacity, let the ray continue
		Texture *tex_opacity = material->get_texture(Material::kOpacityMapSlot);

		/*if(tex_opacity != NULL && opaqueLevel > 0) {
			ray.origin = p + Vector3(0.001f,0.001f,0.001f); //from hit point, in the same direction
			return RayTrace(scene, ray, surfaces, lights, it, maxIt, opaqueLevel-1); //return color after opaque material
		}*/

		bool lastIteration = currentDepth >= maxDepth;

		Vector3 geometryNormal = -Vector3(rtc_ray.Ng);
		geometryNormal.Normalize();
				
		Vector3 normal = triangle.normal(rtc_ray.u, rtc_ray.v);
		
		for(std::vector<OmniLight *>::const_iterator iter = lights->begin(); iter != lights->end(); iter++) {
			OmniLight * light = *iter;
			RTCRay shadowRay = Ray::createShadowRay(p, light->position - p);
			rtcOccluded(*scene, shadowRay);
			light->occlusion = ((shadowRay.geomID == 0) ? 1.f : 0.f);	
		}

		Vector3 lastIterRgb = Vector3(0.f, 0.f, 0.f);
		if( ! lastIteration) {
			Vector3 reflectedRayDirection = p.Reflect(normal);
			reflectedRayDirection.Normalize();

			Ray reflectedRay(p, reflectedRayDirection); 

			lastIterRgb = this->trace(reflectedRay);
		}

		rgb = material->reflectivity * PhongShader(normal, p, texCoord, rtc_ray.dir,material) + lastIterRgb;
	}

	return rgb;
}